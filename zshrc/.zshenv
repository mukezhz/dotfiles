# Initialize $PATH with system and user binaries.
export PATH="$HOME/.bin:$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH"

# Locale.
export LANG="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"

# Term.
export TERM="xterm-256color"

# Default Editor.
export EDITOR="vim"

# Default Pager.
export PAGER="less"

# Zsh.
if [ "$(ps -p $$ -ocomm=)" = "zsh" ]; then
    # Oh My Zsh.
    export ZSH="$HOME/.oh-my-zsh"
    # Set a character at the end of partial lines to none.
    export PROMPT_EOL_MARK=""
    # History settings.
    export HISTORY_IGNORE="([bf]g|c|clear|e|exit|h|history|incognito|l|l[adfls]|pwd|z)"
fi

# Bash.
if [ "$(ps -p $$ -ocomm=)" = "bash" ]; then
    # History settings.
    export HISTTIMEFORMAT="%F %T "
    export HISTCONTROL=ignoredups
    export HISTIGNORE="[bf]g:c:clear:e:exit:h:history:incognito:l:l[adfls]:pwd"
    export HISTSIZE=10000
    # Save and reload the history after each command finishes in another bash session.
    export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
fi

SCRIPT_DIR=/usr/lib/i3blocks


#####################################################
#            CUSTOM APP EXPORTS                     #
#####################################################
# code for nvm
#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
#export NODE_DIR="$HOME/.nvm/versions/node/v12.18.2/bin"
#export PATH="$PATH:$HOME/.nvm/versions/node/v12.18.2/bin:$NVM_DIR"
# capslock is also control now
# /usr/bin/setxkbmap -option "caps:swapctrl"
#

#Flutter Development
##android home
#export ANDROID_HOME=~/flutter/Android
#export PATH=$ANDROID_HOME/tools:$PATH
#export PATH=$ANDROID_HOME/tools/bin:$PATH
#export PATH=$ANDROID_HOME/platform-tools:$PATH

##android sdk root
#export ANDROID_SDK_ROOT=$ANDROID_HOME
#export PATH=$ANDROID_SDK_ROOT:$PATH

##flutter
#export FLUTTER_HOME=~/flutter/flutter
#export PATH=$FLUTTER_HOME/bin:$PATH

##gradle
#export GRADLE_HOME=~/flutter/gradle
#export PATH=$GRADLE_HOME/bin:$PATH

##Android-studio
#export PATH="/home/willsir/flutter/android-studio/bin/:$PATH"

#custom path for bspwm

#source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
#[[ ! -f ~/.p10k.zsh ]] || source ~

#export TERMINAL=/usr/bin/qterminal
#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
#[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# powerline-daemon -q
# POWERLINE_BASH_CONTINUATION=1
# POWERLINE_BASH_SELECT=1
# . /usr/share/powerline/bindings/zsh/powerline.zsh
#

# DENO
#export DENO_INSTALL="$HOME/.deno"
#export PATH="$DENO_INSTALL/bin:$PATH"

# JUPYTER NOTEBOOK
#export PATH="$HOME/.local/bin:$PATH"

# SNAP
export PATH="/var/lib/snapd/snap/bin:$PATH"

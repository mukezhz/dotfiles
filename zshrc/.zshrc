# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

############################################################
#              when terminal is booted
############################################################
# export PF_ASCII="arch"
# pfetch

#archey3
#source ~/.ENV
source ~/.zshenv
# set -o vi
fpath+=~/.zfunc
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# Pure activation
# fpath+=$HOME/.zsh/pure
# autoload -U promptinit; promptinit
# prompt pure
# run: echo $RANDOM_THEME
# My theme
# ZSH_THEME="powerlevel10k/powerlevel10k"
#
#ZSH_THEME="ys"
#ZSH_THEME="lambda"
#ZSH_THEME="random"
#ZSH_THEME="robbyrussell"
#ZSH_THEME="amuse"
#ZSH_THEME="strug"
#ZSH_THEME="bira"
#ZSH_THEME="powerlevel10k"
#ZSH_THEME="oxide"

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# case-sensitive completion.
# CASE_SENSITIVE="true"

# hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=15

# if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# disable marking untracked files under VCS as dirty. 
# This makes repository status check for large repositories faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git zsh-autosuggestions zsh-syntax-highlighting \
        docker-compose dotenv fzf poetry \
        gitignore history pj tmux tmux-cssh \
        urltools web-search z golang archlinux \
        pip pipenv npm pyenv yarn sudo \
)


# User configuration
FRONTEND_SEARCH_FALLBACK='duckduckgo'
# export MANPATH="/usr/local/man:$MANPATH"

ZSH_WEB_SEARCH_ENGINES=(reddit "https://www.reddit.com/search/?q="
                        gitlab "https://gitlab.com/search?q=")
source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Example aliases
# alias ohmyzsh="mate ~/.oh-my-zsh"

#################################################
#         SOURCE
#################################################
source /home/willsir/.config/broot/launcher/bash/br

#export PATH="$PATH:$HOME/go/bin"

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi

export PATH="$HOME/.poetry/bin:$HOME/.local/share/gem/ruby/3.0.0/bin:$PATH"
#export PATH="$HOME/.bin;$HOME/bin;$PATH"

# USER
source ~/.zsh_aliases
eval $(thefuck --alias)
source /usr/share/nvm/init-nvm.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

eval "$(starship init zsh)"

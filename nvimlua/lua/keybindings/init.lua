vim.g.mapleader = ' '
local map = vim.api.nvim_set_keymap
map('n', '<C-h>', '<C-w>h', {noremap = true, silent = false})
map('n', '<C-l>', '<C-w>l', {noremap = true, silent = false})
map('n', '<C-j>', '<C-w>j', {noremap = true, silent = false})
map('n', '<C-k>', '<C-w>k', {noremap = true, silent = false})
map('n', '<C-Right>', '<C-w><', {noremap = true, silent = false})
map('n', '<C-Left>', '<C-w>>', {noremap = true, silent = false})
map('n', '<C-+>', '<C-w>+', {noremap = true, silent = false})
map('n', '<C-->', '<C-w>-', {noremap = true, silent = false})
map('n', '<C-=>', '<C-w>=', {noremap = true, silent = false})

--[[ map('i', 'jk', '<ESC>', {noremap = true, silent = false})
map('i', 'kj', '<ESC>', {noremap = true, silent = false}) ]]

map('n', '<leader>e', ':NvimTreeToggle<CR>', {noremap = true, silent = true})
map('n', '<leader><CR>', ':FZF<CR>', {noremap = true, silent = true})

map('v', '<', '<gv', {noremap = true, silent = false})
map('v', '>', '>gv', {noremap = true, silent = false})

map('n', '<M-up>', ':m-2<CR>', {noremap = false, silent = true})
map('n', '<M-down>', ':m+<CR>', {noremap = false, silent = true})
map('i', '<M-up>', '<C-O>:m-2<CR>', {noremap = false, silent = true})
map('i', '<M-down>', '<C-O>:m+<CR>', {noremap = false, silent = true})

map('i', '<C-\\>', '<C-o>gcc', {noremap = false, silent = true})


require('kommentary.config').use_extended_mappings()

vim.api.nvim_set_keymap("n", "<C-\\>", "<Plug>kommentary_line_default", {})
vim.api.nvim_set_keymap("n", "<C-|>", "<Plug>kommentary_motion_default", {})
vim.api.nvim_set_keymap("v", "<C-\\>", "<Plug>kommentary_visual_default", {})


require('plugins')
require('lsp-installer-config')
require('lsp')
require('keybindings')
require('options')
require('nvim-tree-config')
require('lualine-config')
require('bufferline-config')
require('treesitter-config')
require('autopairs-config')
require('whichkey-config')
require('telescope-config')
require('telescope').load_extension('fzf')
require('kommentary-config')
-- require('lsp')
-- require('lspkind')

-- Example config in Lua
vim.g.tokyonight_style = "storm"
vim.g.tokyonight_italic_functions = true

vim.cmd([[
if &term == "alacritty"        
  let &term = "xterm-256color"
endif
]])

-- Load the colorscheme
-- Example config in Lua
vim.g.tokyonight_style = "night"
vim.g.tokyonight_italic_functions = true
vim.g.tokyonight_sidebars = { "qf", "vista_kind", "terminal", "packer" }
vim.g.tokyonight_lualine_bold = true
vim.g.tokyonight_day_brightness = 0.6
vim.g.tokyonight_italic_functions = true
vim.g.tokyonight_italic_variables = true
-- Change the "hint" color to the "orange" color, and make the "error" color bright red
vim.g.tokyonight_colors = { hint = "orange", error = "#ff0000" }

-- Load the colorscheme
vim.cmd[[colorscheme tokyonight]]

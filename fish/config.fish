if status is-interactive
  fish_config prompt choose informative_vcs
  set fish_greeting
  fish_add_path -aP ~/.local/bin/
  fish_add_path -aP /snap/bin/

  alias pro="cd $HOME/Programming"
  alias cs="cd $HOME/CollegeCourse/project/cs"
  alias fsh="cd $HOME/.config/fish"
  alias nv="cd $HOME/.config/nvim"
  alias ls=exa
  export VISUAL=vim
  export EDITOR=vim

  # eval (ssh-agent -c)
  clear
  # starship init fish | source
end

